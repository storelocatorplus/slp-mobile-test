require 'rho'
require 'rho/rhocontroller'
require 'rho/rhoerror'
require 'helpers/browser_helper'

class SettingsController < Rho::RhoController
  include BrowserHelper
  
  def index    
    @msg = @params['msg']
    if System::get_property('platform') == 'APPLE'
       $app_store_url = Rho::RhoConfig.apple_url
    else 
      if System::get_property('platform') == 'ANDROID'
        $app_store_url = Rho::RhoConfig.android_url
      end
    end      
    render
  end

  def login
    @msg = @params['msg']
    render :action => :login
  end

  def login_callback
    errCode = @params['error_code'].to_i
    if errCode == 0
      # run sync if we were successful
      WebView.navigate Rho::RhoConfig.options_path
      SyncEngine.dosync
    else
      if errCode == Rho::RhoError::ERR_CUSTOMSYNCSERVER
        @msg = @params['error_message']
      end
        
      if !@msg || @msg.length == 0   
        @msg = Rho::RhoError.new(errCode).message
      end
      
      WebView.navigate( (url_for :action => :login), :query => {:msg => @msg} )
    end  
  end
end

require 'rho/rhocontroller'
require 'helpers/browser_helper'
require 'json'

class LocationController < Rho::RhoController
  include BrowserHelper

  #-----------------------------
  #
  # GET /Location
  def index
      $prev_lats = 0
      $prev_longs = 0
    
      # check if we know our position   
      if !GeoLocation.known_position?
        # wait till GPS receiver acquire position
        GeoLocation.set_notification( url_for(:action => :geo_callback), "", 1)
        WebView.navigate(url_for(:action => :wait))
      else
        # show position
        #app_info "*** storelocations() : position GOOD"
        WebView.navigate(url_for(:action => :asynchttp))
      end
  end
  
  #-----------------------------
  #
  def geo_callback
      #app_info "*** geocallback() :"
      
      # navigate to `show_location` page if GPS receiver acquire position  
      if @params['known_position'].to_i != 0 && @params['status'] =='ok'    
        WebView.navigate(url_for(:action => :asynchttp))
          
      # still waiting...
      else 
        WebView.navigate(url_for(:action => :wait))
      end  
  end
  
  #-----------------------------
  #
  def asynchttp
    @lat = 32.100
    @long = -78.910
    @newcoordinates = false
    
    #app_info "*** asynchttp() : "

    if GeoLocation.known_position?
      @lat = GeoLocation.latitude
      @long = GeoLocation.longitude
      GeoLocation.turnoff
    end
    
    $lats = @lat.to_s
    $longs = @long.to_s
    
    if ($lats != $prev_lats)
      $prev_lats = $lats
      @newcoordinates = true
    end
    
    if ($longs != $prev_longs)
      $prev_longs = $longs
      @newcoordinates = true
    end
    
    if (@newcoordinates)
      #app_info "*** asynchttp() : lat " + $lats + " long: " + $longs    
      $url = 'http://' + Rho::RhoConfig.vnh_url + '&action=csl_get_locations&callback=?&lat='+$lats+'&lng='+$longs
      Rho::AsyncHttp.get(
         :url => $url,
         :callback => (url_for :action => :httpget_callback),
         :callback_param => ""         
        )
    end
  end
    
  #-----------------------------
  #
  def httpget_callback
    #app_info "*** httpget_callback() : "
    
    $httpresult = @params['body']
    $parsedresult = $httpresult.slice(2..($httpresult.length-3))
    $jsonresult = Rho::JSON.parse($parsedresult)    
    
    # $jsonresult {
    #    "success"=>true, 
    #    "response"=>[
    #       {"id"=>"1", "name"=>"Cyber Sprocket Labs", "address"=>"359 Wando Place Drive", "address2"=>"Suite D", "city"=>"Mount Pleasant", "state"=>"SC", "zip"=>"29464", "lat"=>"32.8429790", "lng"=>"-79.8732519", "description"=>"", "url"=>"", "sl_pages_url"=>"http://www.dutchbulb.com/store_page/cyber-sprocket-labs/", "email"=>"", "hours"=>"", "phone"=>"", "units"=>"miles", "image"=>"", "distance"=>"0.0146953524627773", "tags"=>""}, 
    #       {"id"=>"3", "name"=>"Abide-A-While Garden Center", "address"=>"1460 U.S. 17", "address2"=>"", "city"=>"Mount Pleasant", "state"=>"SC", "zip"=>"", "lat"=>"32.8161409", "lng"=>"-79.8464454", "description"=>"", "url"=>"http://abideawhilegardencenter.com/", "sl_pages_url"=>"http://www.dutchbulb.com/store_page/abide-a-while-garden-center/", "email"=>"", "hours"=>"", "phone"=>"", "units"=>"miles", "image"=>"", "distance"=>"2.4127851206401", "tags"=>""}, 
    #       {"id"=>"5", "name"=>"Lowe&#039;s Home Improvement", "address"=>"1104 Market Center Boulevard", "address2"=>"", "city"=>"Mount Pleasant", "state"=>"SC", "zip"=>"", "lat"=>"32.8293685", "lng"=>"-79.8344192", "description"=>"", "url"=>"http://www.lowes.com/", "sl_pages_url"=>"http://www.dutchbulb.com/store_page/lowes-home-improvement/", "email"=>"", "hours"=>"", "phone"=>"", "units"=>"miles", "image"=>"", "distance"=>"2.42988707376432", "tags"=>""}
    #       ]
    #     }    
   $result={}
    $jsonresult['response'].each do |item|
      $result[item["id"].to_s]=item
    end
    
    # @result {
    #"1"=>{
    #    "id"=>"1", "name"=>"Cyber Sprocket Labs", "address"=>"359 Wando Place Drive", "address2"=>"Suite D", "city"=>"Mount Pleasant", "state"=>"SC", "zip"=>"29464", "lat"=>"32.8429790", "lng"=>"-79.8732519", "description"=>"", "url"=>"", "sl_pages_url"=>"http://www.dutchbulb.com/store_page/cyber-sprocket-labs/", "email"=>"", "hours"=>"", "phone"=>"", "units"=>"miles", "image"=>"", "distance"=>"0.0146953524627773", "tags"=>""
    #   },
    # "3" => {
    #     "id"=>"3", "name"=>"Abide-A-While Garden Center", "address"=>"1460 U.S. 17", "address2"=>"", "city"=>"Mount Pleasant", "state"=>"SC", "zip"=>"", "lat"=>"32.8161409", "lng"=>"-79.8464454", "description"=>"", "url"=>"http://abideawhilegardencenter.com/", "sl_pages_url"=>"http://www.dutchbulb.com/store_page/abide-a-while-garden-center/", "email"=>"", "hours"=>"", "phone"=>"", "units"=>"miles", "image"=>"", "distance"=>"2.4127851206401", "tags"=>""    
    #   },
    # "5" => {
    #     "id"=>"5", "name"=>"Lowe&#039;s Home Improvement", "address"=>"1104 Market Center Boulevard", "address2"=>"", "city"=>"Mount Pleasant", "state"=>"SC", "zip"=>"", "lat"=>"32.8293685", "lng"=>"-79.8344192", "description"=>"", "url"=>"http://www.lowes.com/", "sl_pages_url"=>"http://www.dutchbulb.com/store_page/lowes-home-improvement/", "email"=>"", "hours"=>"", "phone"=>"", "units"=>"miles", "image"=>"", "distance"=>"2.42988707376432", "tags"=>""
    #   }
    # } 
    WebView.navigate((url_for :action => :view_http))    
  end

  #-----------------------------
  #  
  def view_http    
    render :back => :close
  end

  #-----------------------------
  #  
  def wait    
    render :back => :close
  end  
    
end

